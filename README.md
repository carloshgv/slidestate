Slideshow framework built with HTML5/CSS3/Javascript. [See the demo](http://awebde.ch/slidestate/demo.html)

Features
------------
* Visual themes based on CSS
* Built-in support for simple in-slide transitions
* Javascript controllers for more complex slides
* Plugin support