$(function() {
	console.log('demo');
	slidestate.init('container');
	slidestate.plugin(progressBar, {startActive:true});
	slidestate.plugin(slideNumber, {startActive:false});
	slidestate.plugin(index);
	slidestate.plugin(mousewheel);
	
	slidestate.controller('plots', function(slide) {
		return {
			enter: function() {
				console.log('-- enter plot slide');

				var data = [
					['Heavy Industry', 12],['Retail', 9], ['Light Industry', 14], 
					['Out of home', 16],['Commuting', 7], ['Orientation', 9]
				];
				var plot1 = $.jqplot ('plots_pieChart', [data], { 
					seriesDefaults: {
						// Make this a pie chart.
						renderer: jQuery.jqplot.PieRenderer, 
						rendererOptions: {
							// Put data labels on the pie slices.
							// By default, labels show the percentage of the slice.
							showDataLabels: true
						}
					}, 
					legend: { show:true, location: 'e' },
					grid : {
						background : 'transparent',
						drawBorder : false,
						shadow : false
					}			
				});
			},
			leave: function() {
				console.log('-- leave plot slide');
				setTimeout(function() {
					$('#plots_pieChart *').remove();
				}, 1000);
			}
		}
	});
	slidestate.controller('programmable', function(slide) {
		return {
			enter : function() {
				slide.find('[step]').addClass('visible');
			},
			steps : {
				1: {
					in: function(elements) {
						console.log('prog step 1 in');
						elements.addClass('move-1');
					},
				},
				2: {
					in: function(elements) {
						console.log('prog step 2 in');
						elements.addClass('move-2');
					},
				},
				3: {
					in: function(elements) {
						console.log('prog step 3 in');
						elements.addClass('move-3');
					},
				},
			},
			leave : function() {
				slide.find('[step]').removeClass('move-1 move-2 move-3');
			}
		};
	})

	slidestate.start('start');
});
