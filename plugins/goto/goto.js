var goTo = (function() {
	var template = '<div class="goto-destination-container"><input tabindex="0" type="text" class="goto-destination"/></div>';
	var input = $(template);

	var onKey = function(event) {
		switch(event.which) {
			case 27: // ESC
				$('.goto-destination-container').remove();
				event.preventDefault();
				slidestate.enableKeyEvents();
				break;
			case 13: // ENTER
				slidestate.navigate.to($('.goto-destination').val());
				$('.goto-destination').val('');
				$('.goto-destination-container').remove();
				event.preventDefault();
				slidestate.enableKeyEvents();
				break;
		}
	};

	var onGoto = function(event) {
		event.preventDefault();
		$('body').append(input);
		$('.goto-destination').focus();
		$('.goto-destination').keydown(onKey);
		slidestate.disableKeyEvents();
	};

	return {
		name: 'goTo',
		init: function(config) {
			slidestate.onKey(71, onGoto); // G
		}
	}
})();
