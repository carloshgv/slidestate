var mousewheel = (function() {
	$(window).bind("mousewheel", function(ev) {
		if(ev.originalEvent.wheelDelta > 0)
			$(document).trigger('prev');
		else if(ev.originalEvent.wheelDelta < 0)
			$(document).trigger('next');
	});

	return {};
})();
