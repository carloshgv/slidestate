var progressBar = (function() {
	var template = '<div id="pb-bar-container"><span id="pb-bar"/></div>';
	var active;

	var bgX = 0;
	function animateProgressBar() {
		$('#pb-bar').css('background-position-x', bgX);
		bgX = (bgX + 0.3)%186;
		if(active)
			window.requestAnimationFrame(animateProgressBar);
	}
	return {
		name: 'progressBar',
		init: function(config) {
			active = config ? config.active : true;
			$('body').append(template);
			$('#pb-bar').css('width', slidestate.progress() + '%');
			$('#pb-bar-container').css('display', active?'block':'none');
			if(active)
				animateProgressBar();
			slidestate.onKey(80, function() {
				active = !active;
				$('#pb-bar-container').css('display', active?'block':'none');
				if(active)
					animateProgressBar();
			})
		},
		update: function() {
			console.log(slidestate.progress() + '%');
			$('#pb-bar').css('width', slidestate.progress() + '%');
		}
	}
})();
