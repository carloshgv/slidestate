var slideNumber = (function(config) {
	var template = '<div id="sl-number"><span class="sl-current"/><span class="sl-total"/></div>';
	var active;

	return {
		init: function(config) {
			active = config ? config.active : true;
			$('body').append(template);
			$('#sl-number').css('display', active?'block':'none');
			$('#sl-number .sl-current').html(slidestate.slides.position());
			$('#sl-number .sl-total').html('/' + slidestate.slides.total());
			slidestate.onKey(78, function() {
				active = !active;
				$('#sl-number').css('display', active?'block':'none');
			})
		},
		update: function() {
			$('#sl-number .sl-current').html(slidestate.slides.position());
		}
	}
})();
