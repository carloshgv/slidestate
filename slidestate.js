/*
 *
 */
console.log('starting');
var slidestate = (function($) {
	/* private */
	var slides = [];
	var nameToIndex = {};
	var current = 0;
	var last = 0;
	var controllers = {};
	var plugins = [];
	var pluginConfig = {};
	var inTransition = 0;

	var keyController = {
		33: function() {
			$(document).trigger('prev');
		},
		37: function() {
			$(document).trigger('prev');
		},
		38: function() {
			$(document).trigger('prev');
		},
		34: function() {
			$(document).trigger('next');
		},
		39: function() {
			$(document).trigger('next');
		},
		40: function() {
			$(document).trigger('next');
		},
		190: function() {
			if(document.webkitFullscreenElement)
				document.webkitExitFullscreen();
			else
				document.getElementsByTagName('html')[0].webkitRequestFullscreen();
		},
		70: function() {
				this[190]();
		}
	};

	function forward(from, to) {
		inTransition = 2;
		from.addClass('leave');
		from.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='leave') {
				from.removeClass('leave');
				from.removeClass('visible');
				inTransition--;
				from.off('webkitAnimationEnd');
			}
		});
		to.addClass('visible');
		to.addClass('enter');
		to.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='enter') {
				to.removeClass('enter');
				inTransition--;
				to.off('webkitAnimationEnd');
			}
		});
	};

	function reverse(from, to) {
		inTransition = 2;
		from.css('-webkit-animation-direction', 'reverse');
		from.addClass('enter');
		from.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='enter') {
				from.removeClass('enter');
				from.removeClass('visible');
				from.css('-webkit-animation-direction', '');
				inTransition--;
				from.off('webkitAnimationEnd');
			}
		});
		to.addClass('visible');
		to.css('-webkit-animation-direction', 'reverse');
		to.addClass('leave');
		to.on('webkitAnimationEnd', function(e) {
			if(e.originalEvent.animationName=='leave') {
				to.removeClass('leave');
				to.css('-webkit-animation-direction', '');
				inTransition--;
				to.off('webkitAnimationEnd');
			}
		});
	};

	function next() {
		if(inTransition) return;
		var current_step = slides[current].data('current_step');
		if(current_step < slides[current].data('steps')) {
			slides[current].data('current_step', ++current_step);
			if((current_step>1)&&(((controllers[slides[current].attr('id')]||{}).steps||{})[current_step-1]||{}).out)
				controllers[slides[current].attr('id')].steps[current_step-1].out(
					slides[current].find('[step='+(current_step-1)+']')
				);
			if((((controllers[slides[current].attr('id')]||{}).steps||{})[current_step]||{}).in)
				controllers[slides[current].attr('id')].steps[current_step].in(
					slides[current].find('[step='+current_step+']')
				);
			else
				slides[current].find('[step]').filter(function() {
					return $(this).attr('step') <= current_step; }).addClass('visible');
		}
		else if(current < last) {
			slides[current].data('current_step', current_step);
			window.location.hash = slides[current+1].attr('id');
		}
	};

	function prev() {
		if(inTransition) return;
		var current_step = slides[current].data('current_step');
		if(current_step > 0) {
			slides[current].data('current_step', --current_step);
			if((current_step<slides[current].data('steps'))&&
			  (((controllers[slides[current].attr('id')]||{}).steps||{})[current_step+1]||{}).out)
				controllers[slides[current].attr('id')].steps[current_step+1].out(
					slides[current].find('[step='+(current_step+1)+']')
				);
			else
				slides[current].find('[step]').filter(function() {
					return $(this).attr('step') > current_step; }).removeClass('visible');

			if((((controllers[slides[current].attr('id')]||{}).steps||{})[current_step]||{}).in)
				controllers[slides[current].attr('id')].steps[current_step].in(
					slides[current].find('[step='+current_step+']')
				);
		}
		else if(current > 0) {
			slides[current].data('current_step', current_step);
			window.location.hash = slides[current-1].attr('id');
		}
	};

	function keyListener(event) {
		if(keyController[event.which]) keyController[event.which](event);
	};

	/* public */
	function go(to) {
		if(inTransition) return;
		var from = slides[current].attr('id');
		var toIndex = nameToIndex[to];
		if(toIndex === undefined) {
			window.alert('wrong slide');
			return;
		}

		if(toIndex < current) {
			reverse(slides[current], slides[toIndex]);
			current = toIndex;
		}
		else if(toIndex > current) {
			forward(slides[current], slides[toIndex]);
			current = toIndex;
		}
		
		if((controllers[from]||{}).leave)
			controllers[from].leave(slides[current]);
		if((controllers[to]||{}).enter)
			controllers[to].enter(slides[nameToIndex[to]]);

		plugins.forEach(function (p) { if(p.update) p.update(); });
	};

	function init(root) {
		$('#' + root + ' section').each(function(index) {
			nameToIndex[$(this).attr('id')] = index;
			slides[index] = $(this);
			var maximum = null;
			$(this).find('[step]').each(function() {
				var value = parseFloat($(this).attr('step'));
				maximum = (value > maximum) ? value : maximum;
			});
			$(this).data('steps', maximum || 0);
			$(this).data('current_step', 0);
		});
		last = slides.length-1;
		inTransition = 0;
		$(document).keydown(keyListener);
		$(window).on('popstate', function(e) {
			console.log(window.location.hash.slice(1));
			var slideRequest = window.location.hash.slice(1);
			if(!isNaN(parseInt(slideRequest))) {
				var destinationId = slides[Math.min(Math.max(parseInt(slideRequest)-1,0), slides.length-1)].attr('id');
				go(destinationId);
				history.replaceState('', '', '#' + destinationId);
			}
			else
				go(slideRequest||slides[0].attr('id'));
		});

		$(document).on('next', next);
		$(document).on('prev', prev);
	};

	function controller(slide, ctrl) {
		controllers[slide] = ctrl(slides[nameToIndex[slide]]);
	};

	function plugin(plugin, config) {
		plugins.push(plugin);
		pluginConfig[plugin.name] = config;
	}

	function start(defaultSlide) {
		var slideRequest = window.location.hash.slice(1);
		if(!isNaN(parseInt(slideRequest)))
			current = Math.min(Math.max(parseInt(slideRequest)-1,0),slides.length-1);
		else
			current = nameToIndex[slideRequest || defaultSlide];

		slides[current].addClass('visible');
		if((controllers[slides[current].attr('id')]||{}).enter)
			controllers[slides[current].attr('id')].enter(slides[current]);

		plugins.forEach(function(p) { if(p.init) p.init(pluginConfig[p.name]); });
	};

	function onKey(key, listener) {
		keyController[key] = listener;
	}

	return {
		navigate : {
			to: function(dst) { window.location.hash = dst; },
			next: next,
			prev: prev
		},
		slides : {
			total: function() { return slides.length; },
			get: function() { return slides; },
			position: function() { return current+1; }
		},
		progress: function() { return (current/(slides.length-1))*100; },
		disableTransitions: function() { $(document).off('next', next); $(document).off('prev', prev); },
		enableTransitions: function() { $(document).on('next', next); $(document).on('prev', prev); },
		disableKeyEvents: function() { $(document).off('keydown', keyListener); },
		enableKeyEvents: function() { $(document).on('keydown', keyListener); },
		init: init,
		controller: controller,
		plugin: plugin,
		onKey: onKey,
		start: start
	};
})(jQuery);
